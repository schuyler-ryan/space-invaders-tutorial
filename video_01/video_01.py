## video_01
#  This video will focus on creating the game window and gameplay loop in Pygame.
#  These features will be present in any pygame game and serve as an introduction
#  for using the module.

# First we must import the pygame module
import pygame

## Then we start by initializing some important variables

#  This variable stores the width of the game window in pixels
window_width = 800

#  This variable stores the height of the game window in pixels
window_height = 600

# Now let's render the window

#  This will store the window object for us
window = pygame.display.set_mode((window_width, window_height))
