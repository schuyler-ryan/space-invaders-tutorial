import pygame

# A class to store information about the game state
class GameInfo:
    def __init__(self, window_width, window_height):
        pass

# A class to store information and dynamic behavior of the player
class Player:
    def __init__(self, window_width, window_height):
        pass

# A class to store information and dynamic behavior of the enemies
class Enemy:
    def __init__(self, window_width, window_height):
        pass
