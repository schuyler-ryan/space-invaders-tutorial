## video_02
#  This video will focus on creating the GameInfo, Player, and Enemy classes.
#  These classes will contain the information and dynamic behaviour of the entities in our game.
#  First we must import the pygame module
import pygame
import entities
## Then we start by initializing some important variables
#  This variable stores the width of the game window in pixels
window_width = 900
#  This variable stores the height of the game window in pixels
window_height = 600
## Now let's render the window
#  Set the caption and icon
icon = pygame.image.load("images/ufo.png")
title = "Space Invaders"
pygame.display.set_icon(icon)
pygame.display.set_caption(title)
#  This will store the window object for us
window = pygame.display.set_mode((window_width, window_height))

# Create the objects
game_info = entities.GameInfo(window_width, window_height)
player = entities.Player(window_width, window_height)
enemy = entities.Enemy(window_width, window_height)

## Gameplay Loop
while True:
    # Draw the objects to the screen
    window.blit(game_info.background, (0, 0))
    window.blit(player.image, player.location)
    window.blit(enemy.image, enemy.location)

    # Handle user input
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit(0)
    pygame.display.update()
