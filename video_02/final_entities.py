import pygame

# A class to store information about the game state
class GameInfo:
    def __init__(self, window_width, window_height):
        background = pygame.image.load('images/galaxy.jpg').convert()
        self.background = pygame.transform.scale(background, (window_width, window_height))

# A class to store information and dynamic behavior of the player
class Player:
    def __init__(self, window_width, window_height):
        self.image = pygame.image.load('images/space-ship.png')
        self.height = 64
        self.width = 64
        starting_location = ((window_width - self.width) / 2, window_height - self.height)
        self.location = starting_location

# A class to store information and dynamic behavior of the enemies
class Enemy:
    def __init__(self, window_width, window_height):
        self.image = pygame.image.load('images/alien-ship.png')
        self.height = 64
        self.width = 64
        starting_location = ((window_width - self.width) / 2, (window_height - self.height) / 2)
        self.location = starting_location
