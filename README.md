# Space Invaders Tutorial

Repo for containing project code on the space invaders tutorial series

## Breakdown

Several folders here, one for each video. Within the folder is a python file called "video_XX.py" that contains the template code that will be used in the tutorial for the viewer to copy from. Additionally, any other media files that are used will be included. There will also be a file, or several, called "final_XXXXX.py" which is a version of whatever "XXXXX.py" file we're working on in the series that results from the end of the video. For example, "final_main.py" in /video_01/ is the "main.py" resulting from the end of video_01. I don't know yet whether or not this file should remain in the directory on release... probably not. I am not sure how to format files for further videos. I'm thinking just including more template code that is seperate from the "final_XXXXX.py" files from the previous video. I want those to be a way of tracking the progress made in each video instead of just delivering a final product to the viewer. Otherwise they could just skip to the last directory and copy the whole file and get the game... really not in the spirit of it.

## Usage
```
cd existing_repo
git remote add origin https://gitlab.com/schuyler-ryan/space-invaders-tutorial.git
git branch -M main
git push -uf origin main
```
"final_XXXXX.py" can be run on PyCharm (or any other environment, but PyCharm will be used for the series). Other .py files are just template code and are not guarenteed to run at all

## Support
If you have issues or questions, create an issue ticket on the right sidebar with a description of your question/concern. Or slack me @Schuyler Ryan 

## Project status
Currently, the first video is created, but not filmed. The second video has not been started
